variable "vpc_cidr_block" {}
variable "private_subnet_cidr_blocks" {}
variable "public_subnet_cidr_blocks" {}


provider "aws" {
  region = "us-east-1"
}

#define a data to query AZ 
data "aws_availability_zones" "azs" {

}

module "myapp-vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.19.0"
  # mondatory inputs
  name = "myapp-vpc"
  cidr = var.vpc_cidr_block
  #defining a private subnet & a public subnet for each of the AZs 
  private_subnets = var.private_subnet_cidr_blocks
  public_subnets  = var.public_subnet_cidr_blocks
  azs             = data.aws_availability_zones.azs.names

  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true

  tags = {
    #these tags are here to help the cloud control manager to indentify which vpc & subnets it should connect to  
    "kubernetes.io/cluster/myapp-eks-cluster " = "shared"
  }
  #elastic load balancer is the entry point , so we need to define the one that is open for external reqs and the one that is not
  public_subnet_tags = {
    "kubernetes.io/cluster/myapp-eks-cluster " = "shared"
    "kubernetes.io/role/elb"                   = 1
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/myapp-eks-cluster " = "shared"
    "kubernetes.io/role/internal-elb"          = 1
  }
}



