variable "cluster_name" {}

module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  version         = "19.10.0"
  cluster_name    = "myapp-eks-cluster"
  cluster_version = "1.24" #kubernetes version

  subnet_ids = module.myapp-vpc.private_subnets #syntax : module.nameOfModule.Output
  vpc_id     = module.myapp-vpc.vpc_id

  tags = { #these tags are not required, it's just for us 
    environment = "development"
    application = "myapp"

  }
  #type of node group (grap the code from "readme" section in terraform doc /modules)
  eks_managed_node_groups = {
    #name of the groupe node is dev with 2 instances (type T3)
    dev = {
      min_size     = 1
      max_size     = 3
      desired_size = 3

      instance_types     = ["t3.small"]
      availability_zones = "us-east-1a"
    }
  }
}

#retrieves information about Auto Scaling Groups in AWS that have a specific tag key value matching the var.cluster_name variable.

data "aws_autoscaling_groups" "eks" {
  filter {
    name   = "tag-key"
    values = [var.cluster_name]
  }

}

#output the autoscaling that have a specific tag key value matching the var.cluster_name variable. 
output "autoscaling_group_names" {
  value = data.aws_autoscaling_groups.eks
}
