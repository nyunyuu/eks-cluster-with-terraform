terraform {
  #referencing the s3 bucket as the backend for the tfstate file, the S3 should be created before applying this chunk of config 
  backend "s3" {
    bucket         = "hin-tfstate-bucket"
    key            = "global/s3/terrafome.tfstate" #tree structure in the S3
    region         = "us-east-1"
#    dynamodb_table = "dynamodb-state-locking"
    encrypt        = true
  }
}

resource "aws_s3_bucket" "terraforme_state" {
  bucket = "hin-tfstate-bucket"
      #to ensure to not delete the bucket accidently with terraform destroy
  lifecycle {
    prevent_destroy = true
  }
  versioning {
    enabled = true
  }
  #make sure that the file is encrypted when stored in aws
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {

        sse_algorithm = "AES256"
      }
    }
  }
}

#lock the state file to ensure that two peoples cannot modify state file in the same time 
/*resource "aws_dynamodb_table" "state_locking" {
  hash_key = "LockID"
  name     = "dynamodb-state-locking"
  attribute {
    name = "LockID"
    type = "S"
  }
  billing_mode = "PAY_PER_REQUEST"
}*/
