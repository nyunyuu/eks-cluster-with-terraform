## Demo App
 - The LAB shows how to make an HA infrastructure by deploying 3 EC2 instances (each one in an availability zone) by using EKS module
## Services deployed 
- VPC , Private and public subnets
- NAT gateway
- min and max size of instances desired
- EC2 instances (type t3.micro)
- SG, NACL, rtb , IGW
- S3 bucket, used as backend for terraforme state file (. tfstate)
- Lock the state file to ensure that two peoples cannot modify state file in the same time
- Make sure that the file is encrypted when stored in aws

## Architecture
![image info](images/eks.png)
